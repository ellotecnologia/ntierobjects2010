unit UFuncoes_Gerais_XML;

interface

uses
   SysUtils, MSXML2_TLB;

// Fun��es de manipulacao de XML
procedure Create_Root(var Root_DomDocument: IXMLDOMDocument; Root_Name: String);
procedure Create_Node(var Root_DomDocument: IXMLDOMDocument; var Node :IXMLDOMNode; Node_Name, Node_Value: String);
procedure Append_Child(var DomDocument_Pai, DomDocument_Filho: IXMLDOMDocument);

// Fun��es de String
function ReplaceStr(const S, Srch, Replace: string): string;
function AlinharDireita(Texto: string; Tamanho: Integer): string;
function AlinharEsquerda(Texto: string; Tamanho: Integer): string;
function AlinharCentralizado(Texto: string; Tamanho: Integer): string;
function Spaces(Numero_Espacos: Integer): string;
function RepeatCaracter(Numero: Integer; Caracter: string): string;
function ZerosEsquerda(Texto: string; Tamanho: Integer): string;

implementation

procedure Create_Root(var Root_DomDocument: IXMLDOMDocument; Root_Name: String);
var
   String_XML: String;
begin
   String_XML := '<' + Trim(LowerCase(Root_Name)) + '/>';
   Root_DomDocument.loadXML(String_XML)
end;

procedure Create_Node(var Root_DomDocument: IXMLDOMDocument; var Node :IXMLDOMNode; Node_Name, Node_Value: String);
begin
   Node := Root_DomDocument.createNode(1, Trim(LowerCase(Node_Name)), '');
   Node.text := Trim(Node_Value);
   Root_DomDocument.documentElement.appendChild(Node);
end;

procedure Append_Child(var DomDocument_Pai, DomDocument_Filho: IXMLDOMDocument);
begin
   DomDocument_Pai.documentElement.appendChild(DomDocument_Filho.documentElement);
end;

function ReplaceStr(const S, Srch, Replace: string): string;
var
  I: Integer;
  Source: string;
begin
  Source := S;
  Result := '';
  repeat
    I := Pos(Srch, Source);
    if I > 0 then
    begin
      Result := Result + Copy(Source, 1, I - 1) + Replace;
      Source := Copy(Source, I + Length(Srch), MaxInt);
    end
    else
       Result := Result + Source;
  until I <= 0;
end;

function AlinharDireita(Texto: string; Tamanho: Integer): string;
var
   TamanhoTexto: Integer;

begin
   Texto := Trim(Texto);
   TamanhoTexto := Length(Texto);

   Tamanho := Tamanho - TamanhoTexto;
   if Tamanho > 0 then
      Result := Spaces(Tamanho) + Texto;
end;

function AlinharEsquerda(Texto: string; Tamanho: Integer): string;
var
   TamanhoTexto: Integer;

begin
   Texto := Trim(Texto);
   TamanhoTexto := Length(Texto);

   Tamanho := Tamanho - TamanhoTexto;
   if Tamanho > 0 then
      Result := Texto + Spaces(Tamanho);
end;

function ZerosEsquerda(Texto: string; Tamanho: Integer): string;
var
   TamanhoTexto: Integer;
begin
   Texto := Trim(Texto);
   TamanhoTexto := Length(Texto);

   Tamanho := Tamanho - TamanhoTexto;
   if (Tamanho > 0) and (TamanhoTexto > 0) then
      Result := RepeatCaracter(Tamanho, '0') + Texto
   else
      Result := Texto;
end;

function AlinharCentralizado(Texto: string; Tamanho: Integer): string;
var
   TamanhoTexto, Metade1, Metade2: Integer;

begin
   Texto := Trim(Texto);
   TamanhoTexto := Length(Texto);

   Tamanho := Tamanho - TamanhoTexto;
   Metade1 := Trunc(Tamanho/2);
   Metade2 := Tamanho - Metade1;

   Result := Spaces(Metade1) + Texto + Spaces(Metade2);
end;

function Spaces(Numero_Espacos: Integer): string;
var
   i: integer;
begin
   Result := '';

   if Numero_Espacos > 0 then
   begin
      for i:= 1 to Numero_Espacos do
         Result := Result + ' ';
   end;
end;

function RepeatCaracter(Numero: Integer; Caracter: string): string;
var
   i: integer;
begin
   Result := '';

   if Numero > 0 then
   begin
      for i:= 1 to Numero do
         Result := Result + Caracter;
   end;
end;

end.

