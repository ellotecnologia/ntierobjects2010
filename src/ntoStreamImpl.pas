{********************************************************************}
{*                                                                  *}
{* Autor : Danilo Rocha Valente                                     *}
{* Cria��o : 05/2006                                                *}
{* Descri��o : Classe especializada em serializa��o baseada em XML  *}
{********************************************************************}
{* �ltima revis�o :                                                 *}
{********************************************************************}
unit ntoStreamImpl;

interface

uses
  Classes, TypInfo, Dialogs, SysUtils, MSXML2_TLB, UFuncoes_Gerais_XML, Contnrs,
  ClassManagerImpl, Variants;//, CtrlCollections;

type
  TntoStream = class(TComponent)
  private
    FExecucaoEmCascata: Boolean;
    FShowExportErrors: Boolean;
    FShowImportErrors: Boolean;
    function RecuperarCaracteresInvalidosXML(Texto: string): string;
    function LimparCaracteresInvalidosXML(Texto: string): string;
    class procedure GetPropertyNames(AClass: TClass; var PropertyNames:TStringList);
    procedure SetExecucaoEmCascata(const Value: Boolean);
    procedure SetShowExportErrors(const Value: Boolean);
    procedure SetShowImportErrors(const Value: Boolean);
    function Instancia(Source: WideString): TObject;
    function RetiraAcento(psValorEntrada: string): string;
  protected

  public
    constructor Create(AOwner: TComponent); override;
    function ToXML(AObject: TObject): WideString; virtual;
    procedure FromXML(AObject: TObject; Source: WideString); virtual;
  published
    property ExecucaoEmCascata: Boolean read FExecucaoEmCascata write SetExecucaoEmCascata;
    property ShowExportErrors: Boolean read FShowExportErrors write SetShowExportErrors;
    property ShowImportErrors: Boolean read FShowImportErrors write SetShowImportErrors;
  end;

procedure Register;

implementation

{$R *.dcr}

procedure Register;
begin
  RegisterComponents('NTierObjects', [TntoStream]);
end;

{ TntoStream }

constructor TntoStream.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);

  FExecucaoEmCascata := True;
  FShowExportErrors := False;
  FShowImportErrors := False;
end;

//Se a propriedade ainda n�o estiver instanciada, cria a inst�ncia...
function TntoStream.Instancia(Source: WideString): TObject;
var slAux: TStringList;
    XMLDoc: IXMLDOMDocument;
    AObject: TObject;
    ObjetoAux: IXMLDOMNodeList;
    ItemsAux: IXMLDOMNodeList;
    i: Integer;
    XMLPropertyClass: IXMLDOMDocument;
    Kind: TTypeKind;
    auxClasse: TClass;
    lClassAux: TObject;
    FormatSettings: TFormatSettings;
begin
  auxClasse := nil;
  if (Trim(Source) = '') then Exit;

  //Cria os documentos XML
  XMLDoc := CoDOMDocument.Create;
  XMLPropertyClass := CoDOMDocument.Create;
  //Cria o StringList para carregar o nome das propriedades publicadas
  slAux := TStringList.Create;
  try
    //Carrega o documento XML principal com a String fornecida
    XMLDoc.loadXML(Source);
    //Se a propriedade ainda n�o estiver instanciada, cria a inst�ncia...
    try

      try
        GetPropertyNames(GetClass(AObject.ClassName), slAux);
      except
        if (ShowImportErrors) then
          ShowMessage(Format('N�o foi poss�vel identificar as propriedades da Classe %s ' + #13#10 +
                             'Verifique se a classe est� registrada.', [AObject.ClassName]));
        Exit;
      end;

      //Importa as propriedades publicadas
      for i := 1 to slAux.Count - 1 do begin
          if (GetObjectProp(AObject, slAux[i]) = nil) then
          begin
            lClassAux := GetObjectPropClass(AObject, slAux[i]).Create;
            try
              if lClassAux is TCollection then
                auxClasse := ClassManager.ListClassByName(XMLDoc.documentElement.selectsinglenode('class').text, csServer)
              else
                try
                  auxClasse := FindClass(ObjetoAux.item[0].selectSingleNode('class').text);
                except
                  ClassManager.ListClassByName(XMLDoc.documentElement.selectsinglenode('class').text, csServer);
                end;
            finally
              lClassAux.Free;
            end;

            if (auxClasse = nil) then auxClasse := GetObjectPropClass(AObject, slAux[i]);

            if (auxClasse <> nil) then
              SetObjectProp(AObject, slAux[i], auxClasse.Create)
            else
            begin
              if (ShowImportErrors) then
                ShowMessage(Format('A Classe %s n�o foi encontrada.' + #13#10 +
                                   'Verifique se a classe est� registrada.', [auxClasse.ClassName]));
              Exit;
            end;

          end;
      end;
    Except on e:Exception do
       begin
          ShowMessage('Erro ao instanciar a classe '+auxClasse.ClassName+'! Motivo: '+e.Message);
       end;
    end;

  finally
    //Libera os recursos
    XMLDoc := nil;
    XMLPropertyClass := nil;
    ObjetoAux := nil;
    ItemsAux := nil;
    slAux.Free;
  end;
end;

procedure TntoStream.FromXML(AObject: TObject; Source: WideString);
var slAux: TStringList;
    XMLDoc: IXMLDOMDocument;
    ObjetoAux: IXMLDOMNodeList;
    ItemsAux: IXMLDOMNodeList;
    i: Integer;
    XMLPropertyClass: IXMLDOMDocument;
    Kind: TTypeKind;
    auxClasse: TClass;
    lClassAux: TObject;
    FormatSettings: TFormatSettings;
begin
  auxClasse := nil;
  if (Trim(Source) = '') then
    Exit;
  //Cria os documentos XML
  XMLDoc := CoDOMDocument.Create;
  XMLPropertyClass := CoDOMDocument.Create;
  //Cria o StringList para carregar o nome das propriedades publicadas
  slAux := TStringList.Create;
  try
    //Carrega o documento XML principal com a String fornecida
    XMLDoc.loadXML(Source);
    if ((AObject Is TPersistent) and (AObject.ClassParent<>TCollection)) then
    begin
      //Carrega os nomes das propriedades
      try
        GetPropertyNames(GetClass(AObject.ClassName), slAux);
      except
        if (ShowImportErrors) then
          ShowMessage(Format('N�o foi poss�vel identificar as propriedades da Classe %s ' + #13#10 +
                             'Verifique se a classe est� registrada.', [AObject.ClassName]));
        Exit;
      end;
      //Importa as propriedades publicadas
      for i := 1 to slAux.Count - 1 do
      begin
        //Verifica se a propriedade � uma classe
        if (PropIsType(AObject, slAux[i], tkClass)) then
        begin
          //Carrega os n�s do Texto iniciados com a Tag do nome do atributo
          ObjetoAux := XMLDoc.documentElement.selectNodes(LowerCase(slAux[i]));
          if (ObjetoAux.length = 0) then
            Continue;

          //Carrega os n�s do "corpo" do objeto e se estiver vazio vai para a
          //  pr�xima propriedade
          ObjetoAux := ObjetoAux.item[0].selectNodes('object');
          if (ObjetoAux.length = 0) then
            Continue;

          //Se a propriedade ainda n�o estiver instanciada, cria a inst�ncia...
          try
             if (GetObjectProp(AObject, slAux[i]) = nil) then
             begin
               lClassAux := GetObjectPropClass(AObject, slAux[i]).Create;
               try
                 if lClassAux is TCollection then
                   auxClasse := ClassManager.ListClassByName(XMLDoc.documentElement.selectsinglenode('class').text, csServer)
                 else
                   try
                     auxClasse := FindClass(ObjetoAux.item[0].selectSingleNode('class').text);
                   except
                     ClassManager.ListClassByName(XMLDoc.documentElement.selectsinglenode('class').text, csServer);
                   end;
               finally
                 lClassAux.Free;
               end;

               if (auxClasse = nil) then
                 auxClasse := GetObjectPropClass(AObject, slAux[i]);
               if (auxClasse <> nil) then
                 SetObjectProp(AObject, slAux[i], auxClasse.Create)
               else
               begin
                 if (ShowImportErrors) then
                   ShowMessage(Format('A Classe %s n�o foi encontrada.' + #13#10 +
                                      'Verifique se a classe est� registrada.', [auxClasse.ClassName]));
                 Continue;
               end;
             end;
          Except on e:Exception do
          begin
             ShowMessage('Erro ao instanciar a classe '+auxClasse.ClassName+'! Motivo: '+e.Message);
          end;

          end;

          if (GetObjectProp(AObject, slAux[i]) <> nil) then
          begin
            FromXML((GetObjectProp(AObject, slAux[i], GetObjectPropClass(AObject, slAux[i]))),
                    ObjetoAux.item[0].transformNode(ObjetoAux.item[0]));
          end;
        end
        else
        if (XMLDoc.documentElement.selectSingleNode(LowerCase(slAux[i])) <> nil) then
        begin
          Kind := PropType(AObject, slAux[i]);
          case Kind of
          tkMethod:
          begin
          end;
          tkFloat:
            begin
              GetLocaleFormatSettings(1, FormatSettings);
              FormatSettings.DecimalSeparator := '.';
              SetPropValue(AObject,
                           slAux[i],
                           StrToFloat(XMLDoc.documentElement.selectSingleNode(LowerCase(slAux[i])).text,
                                      FormatSettings));
            end;
            tkEnumeration:
              begin
                if
                  LowerCase(XMLDoc.documentElement.selectSingleNode(LowerCase(slAux[i])).text) = 'true' then
                  SetPropValue(AObject, slAux[i], 1)
                else if
                  LowerCase(XMLDoc.documentElement.selectSingleNode(LowerCase(slAux[i])).text) = 'false' then
                  SetPropValue(AObject, slAux[i], 0)
                else
                  SetPropValue(AObject, slAux[i],
                    StrToInt(XMLDoc.documentElement.selectSingleNode(LowerCase(slAux[i])).text));
              end;
            tkDynArray:
              begin
              end;
          else
            begin
              SetPropValue(AObject,
                slAux[i],
                RecuperarCaracteresInvalidosXML(XMLDoc.documentElement.selectSingleNode(LowerCase(slAux[i])).text));
            end;
          end;
        end;
      end;
    end//if (AObject Is TPersistent) then
    else
      if (AObject Is TCollection) then
      begin
        try
           //L� os n�s de XML contendo os itens (containers) da lista
           ItemsAux := XMLDoc.documentElement.selectNodes('items');
           if (ItemsAux.length > 0) then
             ItemsAux := ItemsAux.item[0].selectNodes('object');

           //Limpa os itens existentes
           for i := 0 to (AObject As TCollection).Count - 1 do
             if ((AObject As TCollection).Items[i] <> nil) then
               TObject((AObject As TCollection).Items[i]).Free;

           (AObject As TCollection).Clear;

           //Instancia os itens e faz a carga dos atributos
           for i := 0 to ItemsAux.length - 1 do begin
             // Instancia o Item
             TCollection(AObject).Add;
             // Carrega os atributos encontrados no XML para o item
             FromXML((AObject As TCollection).Items[(AObject As TCollection).Count - 1], ItemsAux.item[i].transformNode(ItemsAux.item[i]));
           end;
        Except on e:Exception do
           ShowMessage('Erro ao recuperar o objeto '+ItemsAux.item[i].text);

        end;
      end
      else
        if (AObject Is TList) then
        begin
          try
             //L� os n�s de XML contendo os itens (containers) da lista
             ItemsAux := XMLDoc.documentElement.selectNodes('items');
             if (ItemsAux.length > 0) then
               ItemsAux := ItemsAux.item[0].selectNodes('object');

             //Limpa os itens existentes
             for i := 0 to (AObject As TList).Count - 1 do
               if ((AObject As TList).Items[i] <> nil) then
                 TObject((AObject As TList).Items[i]).Free;
             (AObject As TList).Clear;

             //Instancia os itens e faz a carga dos atributos
             for i := 0 to ItemsAux.length - 1 do
             begin
               //Instancia o Item
               TObjectList(AObject).Add(Instancia(ItemsAux.item[i].transformNode(ItemsAux.item[i])) );

               //Carrega os atributos encontrados no XML para o item
               //FromXML((AObject As TList).Items[(AObject As TList).Count - 1], ItemsAux.item[i].transformNode(ItemsAux.item[i]));
               FromXML((AObject As TList).Items[(AObject As TList).Count - 1], ItemsAux.item[i].transformNode(ItemsAux.item[i]));
             end;
          Except on e:Exception do
             ShowMessage('Erro ao recuperar o objeto '+ItemsAux.item[i].text);

          end;
        end;
  finally
    //Libera os recursos
    XMLDoc := nil;
    XMLPropertyClass := nil;
    ObjetoAux := nil;
    ItemsAux := nil;
    slAux.Free;
  end;
end;

class procedure TntoStream.GetPropertyNames(AClass: TClass; var PropertyNames: TStringList);
var
  TypeInfo: PTypeInfo;
  TypeData: PTypeData;
  PropList: PPropList;
  i: Integer;
begin
  if Assigned(PropertyNames) then { check to see if the TStringList is valid }
  begin
    PropertyNames.Clear;
    TypeInfo := AClass.ClassInfo;
    if TypeInfo^.Kind = tkClass then
    begin

      TypeData := GetTypeData(TypeInfo);

      if TypeData^.PropCount > 0 then
      begin
        PropertyNames.Add(TypeInfo^.Name + ':');
        new(PropList);
        GetPropInfos(TypeInfo, PropList);
        for i := 0 to Pred(TypeData^.PropCount) do
          if PropertyNames.IndexOf(PropList^[i]^.Name) < 0 then
            PropertyNames.Add(PropList^[i]^.Name);
        Dispose(PropList)
      end
    end
  end
end; {GetPropertyNames}

function TntoStream.RetiraAcento(psValorEntrada : string) : string;
var
  i : integer;
begin
  for i := 1 to Length(psValorEntrada) do begin

    if psValorEntrada[i]='�' then psValorEntrada[i]:='a' else
    if psValorEntrada[i]='�' then psValorEntrada[i]:='e' else
    if psValorEntrada[i]='�' then psValorEntrada[i]:='i' else
    if psValorEntrada[i]='�' then psValorEntrada[i]:='o' else
    if psValorEntrada[i]='�' then psValorEntrada[i]:='u' else
    if psValorEntrada[i]='�' then psValorEntrada[i]:='A' else
    if psValorEntrada[i]='�' then psValorEntrada[i]:='E' else
    if psValorEntrada[i]='�' then psValorEntrada[i]:='I' else
    if psValorEntrada[i]='�' then psValorEntrada[i]:='O' else
    if psValorEntrada[i]='�' then psValorEntrada[i]:='U' else
    if psValorEntrada[i]='�' then psValorEntrada[i]:='a' else
    if psValorEntrada[i]='�' then psValorEntrada[i]:='e' else
    if psValorEntrada[i]='�' then psValorEntrada[i]:='i' else
    if psValorEntrada[i]='�' then psValorEntrada[i]:='o' else
    if psValorEntrada[i]='�' then psValorEntrada[i]:='u' else
    if psValorEntrada[i]='�' then psValorEntrada[i]:='A' else
    if psValorEntrada[i]='�' then psValorEntrada[i]:='E' else
    if psValorEntrada[i]='�' then psValorEntrada[i]:='I' else
    if psValorEntrada[i]='�' then psValorEntrada[i]:='O' else
    if psValorEntrada[i]='�' then psValorEntrada[i]:='U' else
    if psValorEntrada[i]='�' then psValorEntrada[i]:='a' else
    if psValorEntrada[i]='�' then psValorEntrada[i]:='e' else
    if psValorEntrada[i]='�' then psValorEntrada[i]:='i' else
    if psValorEntrada[i]='�' then psValorEntrada[i]:='o' else
    if psValorEntrada[i]='�' then psValorEntrada[i]:='u' else
    if psValorEntrada[i]='�' then psValorEntrada[i]:='a' else
    if psValorEntrada[i]='�' then psValorEntrada[i]:='o' else
    if psValorEntrada[i]='�' then psValorEntrada[i]:='A' else
    if psValorEntrada[i]='�' then psValorEntrada[i]:='E' else
    if psValorEntrada[i]='�' then psValorEntrada[i]:='I' else
    if psValorEntrada[i]='�' then psValorEntrada[i]:='O' else
    if psValorEntrada[i]='�' then psValorEntrada[i]:='U' else
    if psValorEntrada[i]='�' then psValorEntrada[i]:='A' else
    if psValorEntrada[i]='�' then psValorEntrada[i]:='O' else
    if psValorEntrada[i]='�' then psValorEntrada[i]:='C' else
    if psValorEntrada[i]='>' then psValorEntrada[i]:=' ' else
    if psValorEntrada[i]='<' then psValorEntrada[i]:=' ' else
    if psValorEntrada[i]='�' then psValorEntrada[i]:='c';
  end;

  Result := psValorEntrada;
end;


function TntoStream.LimparCaracteresInvalidosXML(Texto: string): string;
begin
//  Result := StringReplace(Texto, '�', '%I$%', [rfReplaceAll]);
  Result := RetiraAcento(Texto);
end;

function TntoStream.RecuperarCaracteresInvalidosXML(Texto: string): string;
begin
  Result := StringReplace(Texto, '%I$%', '�', [rfReplaceAll]);
end;

procedure TntoStream.SetExecucaoEmCascata(const Value: Boolean);
begin
  FExecucaoEmCascata := Value;
end;

procedure TntoStream.SetShowExportErrors(const Value: Boolean);
begin
  FShowExportErrors := Value;
end;

procedure TntoStream.SetShowImportErrors(const Value: Boolean);
begin
  FShowImportErrors := Value;
end;

function TntoStream.ToXML(AObject: TObject): WideString;
var
  i, j: Integer;
  slAux: TStringList;
  Node: IXMLDOMNode;
  XMLDoc: IXMLDOMDocument;
  XMLProperty: IXMLDOMDocument;
  XMLPropertyClass: IXMLDOMDocument;
  Kind: TTypeKind;
  FormatSettings: TFormatSettings;
begin
  Result := '';

  //Cria os documentos XML
  XMLDoc := CoDOMDocument.Create;
  XMLProperty := CoDOMDocument.Create;
  XMLPropertyClass := CoDOMDocument.Create;
  //Cria o StringList que abrigar� as propriedades publicadas da classe
  slAux := TStringList.Create;
  try
    //Cria o root e o n� com o nome da classe do objeto
    Create_Root(XMLDoc, 'object');
    Create_Node(XMLDoc, Node, 'class', AObject.ClassName);
    //Exporta as propriedades obtidas para o documento XML
    if ((AObject Is TPersistent) and (AObject.ClassParent <> TCollection)) then
    begin
      //Carrega o StringList com os nomes das propriedades da classe
      try
         GetPropertyNames(GetClass(AObject.ClassName), slAux);
      except
        if (ShowExportErrors) then
          ShowMessage(Format('N�o foi poss�vel identificar as propriedades da Classe %s ' + #13#10 +
                             'Verifique se a classe est� registrada.', [AObject.ClassName]));
        Exit;
      end;
      for i := 1 to slAux.Count - 1 do
      begin
        //Verifica se a propriedade � uma classe
        if (PropIsType(AObject, slAux[i], tkClass)) then
        begin
          if ExecucaoEmCascata then
          begin
            //Verifica se a propriedade est� instanciada            
            if (GetObjectProp(AObject, slAux[i]) = nil) then
              Create_Root(XMLPropertyClass, LowerCase(slAux[i]))
            else
            begin
              
               XMLPropertyClass.loadXML('<' + Trim(LowerCase(slAux[i])) + '>' + ToXML(GetObjectProp(AObject, slAux[i])) + '</' + Trim(LowerCase(slAux[i])) + '>');

               XMLDoc.documentElement.appendChild(XMLPropertyClass.documentElement);
            end;
          end; //if ExportAllChildObjects then
        end
        else //if (PropIsType(AObject, slAux[i], tkClass))...
        begin
          //Se a propriedade for um tipo b�sico (string, datetime, integer, etc.) cria um n� com seu valor
          Kind := PropType(AObject, slAux[i]);
          case Kind of
            tkMethod:
            begin
            end;
            tkFloat:
              begin
                GetLocaleFormatSettings(1, FormatSettings);
                FormatSettings.DecimalSeparator := '.';
                Create_Node(XMLDoc,
                  Node,
                  slAux[i],
                  FloatToStr((GetFloatProp(AObject,
                               slAux[i])), FormatSettings));
              end;
            tkEnumeration:
              begin
                if (LowerCase(VarAsType(GetPropValue(AObject, slAux[i]), varString))
                  =
                  'true') or
                  (LowerCase(VarAsType(GetPropValue(AObject, slAux[i]), varString)) =
                  'false') then
                  Create_Node(XMLDoc, Node, slAux[i], VarAsType(GetPropValue(AObject,
                    slAux[i]), varString))
                else
                  Create_Node(XMLDoc, Node, slAux[i], VarAsType(GetPropValue(AObject,
                    slAux[i], False), $0003)); //$0003 = Integer
              end;
            tkDynArray:
              begin
                Create_Root(XMLProperty, slAux[i]);

                try
                  for j := 0 to VarArrayDimCount(GetPropValue(AObject, slAux[i])) - 1
                    do
                    Create_Node(XMLProperty, Node, 'item', GetPropValue(AObject,
                      slAux[i])[j]);
                except
                end;
                XMLDoc.documentElement.appendChild(XMLProperty.documentElement);
              end;
          else
            begin
              Create_Node(XMLDoc,
                Node,
                slAux[i],
                LimparCaracteresInvalidosXML(VarAsType(GetPropValue(AObject,
                slAux[i]), varString)));
            end;
          end; //Case
        end;
      end;
    end//if (AObject Is TPersistent) then
    else
      if (AObject is TCollection) then
      begin
        //Grava a tag que indica o in�cio dos n�s de itens
        Create_Root(XMLPropertyClass, 'items');
        //Cria os n�s de todos os itens que est�o instanciados no objeto de lista
        for i := 0 to (AObject As TCollection).Count - 1 do begin
           try
              XMLProperty.loadXML(ToXML((AObject As TCollection).Items[i]));
              XMLPropertyClass.documentElement.appendChild(XMLProperty.documentElement);
           except
              XMLProperty.loadXML(ToXML((AObject As TCollection).Items[i]));
           end;
        end;
        XMLDoc.documentElement.appendChild(XMLPropertyClass.documentElement);
      end
      else
        if (AObject Is TList) then
        begin
          //Grava a tag que indica o in�cio dos n�s de itens
          Create_Root(XMLPropertyClass, 'items');
          //Cria os n�s de todos os itens que est�o instanciados no objeto de lista
          for i := 0 to (AObject As TList).Count - 1 do
          begin
            XMLProperty.loadXML(ToXML((AObject As TList).Items[i]));

            XMLPropertyClass.documentElement.appendChild(XMLProperty.documentElement);
          end;
          XMLDoc.documentElement.appendChild(XMLPropertyClass.documentElement);
        end;

    //Retorna o XML gerado com o objeto completo
    Result := XMLDoc.documentElement.xml;
  finally
    //libera os recursos
    XMLDoc := nil;
    XMLProperty := nil;
    XMLPropertyClass := nil;
    Node := nil;
    slAux.Free;
  end;
end;

end.
