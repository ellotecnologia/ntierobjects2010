{********************************************************************}
{*                                                                  *}
{* Autor : Danilo Rocha Valente                                     *}
{* Cria��o : 01/2006                                                *}
{* Descri��o : Gerenciador de Classes                               *}
{********************************************************************}
{* �ltima revis�o :                                                 *}
{********************************************************************}
unit ClassManagerImpl;

interface

uses
  Classes, SysUtils;

type
  TClassScope = (csServer, csClient);

  TClientListClasses = Array Of TClass;

  TServerListClasses = Array Of TClass;

  TOnExecute = function: Boolean of object;

  TClassManager = class(TPersistent)
  private
    FClientListClasses: TClientListClasses;
    FServerListClasses: TServerListClasses;
    procedure SetClientListClasses(const Value: TClientListClasses);
    procedure SetServerListClasses(const Value: TServerListClasses);
  protected

  public
    procedure Clear;
    function ClassRegistered(AClassName: String; AClassScope: TClassScope): Boolean;
    function RegisterClientClass(AClientClass: TClass): Boolean;
    function RegisterServerClass(AServerClass: TClass): Boolean;
    function ListClassByName(AClassName: String; AClassScope: TClassScope): TClass;
    property ClientListClasses: TClientListClasses read FClientListClasses write SetClientListClasses;
    property ServerListClasses: TServerListClasses read FServerListClasses write SetServerListClasses;
  end;

  function ClassManager: TClassManager;

var
  FClassManager: TClassManager;

  
implementation

  function ClassManager: TClassManager;
  begin
    if FClassManager = nil then
    begin
      FClassManager := TClassManager.Create;
    end;
    result := FClassManager;
  end;

procedure TClassManager.SetClientListClasses(
  const Value: TClientListClasses);
begin
  FClientListClasses := Value;
end;

function TClassManager.RegisterClientClass(AClientClass: TClass): Boolean;
begin
  if ClassRegistered(AClientClass.ClassName, csClient) then
  begin
    Result := True;
    Exit;
  end;
    //raise Exception.Create(format('A classe Client de lista %s j� est� registrada.',
                                  //[AClientClass.ClassName]));

  SetLength(FClientListClasses, Length(FClientListClasses) + 1);
  FClientListClasses[High(FClientListClasses)] := AClientClass;
  Result := True;
end;

function TClassManager.ClassRegistered(AClassName: String; AClassScope: TClassScope): Boolean;
var
  i: Integer;
begin
  Result := False;

  if AClassScope = csServer then
  begin
    for i:= Low(FServerListClasses) to High(FServerListClasses) do
      if LowerCase(FServerListClasses[i].ClassName) = Lowercase(AClassName) then
      begin
        Result := True;
        Exit;
      end;
  end
  else
  begin
    for i:= Low(FClientListClasses) to High(FClientListClasses) do
      if LowerCase(FClientListClasses[i].ClassName) = Lowercase(AClassName) then
      begin
        Result := True;
        Exit;
      end;
  end;
end;

function TClassManager.ListClassByName(AClassName: String; AClassScope: TClassScope): TClass;
var
  i: Integer;
begin
  Result := nil;

  if AClassScope = csServer then
  begin
    for i:= Low(FServerListClasses) to High(FServerListClasses) do
      if LowerCase(FServerListClasses[i].ClassName) = Lowercase(AClassName) then
      begin
        Result := FServerListClasses[i];
        Exit;
      end;
  end
  else
  begin
    for i:= Low(FClientListClasses) to High(FClientListClasses) do
      if LowerCase(FClientListClasses[i].ClassName) = Lowercase(AClassName) then
      begin
        Result := FClientListClasses[i];
        Exit;
      end;
  end;
end;

procedure TClassManager.SetServerListClasses(
  const Value: TServerListClasses);
begin
  FServerListClasses := Value;
end;

function TClassManager.RegisterServerClass(AServerClass: TClass): Boolean;
begin
  if ClassRegistered(AServerClass.ClassName, csServer) then
  begin
    Result := True;
    Exit;
  end;
    //raise Exception.Create(format('A classe Server de lista %s j� est� registrada.',
                                  //[AServerClass.ClassName]));

  SetLength(FServerListClasses, Length(FServerListClasses) + 1);
  FServerListClasses[High(FServerListClasses)] := AServerClass;
  Result := True;
end;

procedure TClassManager.Clear;
begin
  SetLength(Self.FClientListClasses, 0);
  SetLength(Self.FServerListClasses, 0);
end;

initialization
  ClassManager.Clear;

finalization
  FClassManager.Free;

end.
