unit Unit5;

interface
   uses
      Contnrs, SysUtils, Classes, ntoStreamImpl, ClassManagerImpl;

Type
   TTipoTelefone = (ttCelular, ttFixo);

   TTelefone = Class(TPersistent)
   private
    FNumero: String;
    FIdPessoa: Integer;
    FTipo: TTipoTelefone;
    procedure SetIdPessoa(const Value: Integer);
    procedure SetNumero(const Value: String);
   published
     property IdPessoa:Integer read FIdPessoa write SetIdPessoa;
     property Numero:String read FNumero write SetNumero;
     property Tipo:TTipoTelefone read FTipo write FTipo;
   End;

   TTelefones = Class(TObjectList)
   private
     function GetItem(const index:Integer):TTelefone;
   public
     property Items[const Index:Integer]:TTelefone read GetItem;default;
   End;

   TPessoa = Class(TPersistent)
   private
    FId: Integer;
    FIdade: Integer;
    FFones: TObjectList;
    FNome: String;
    procedure SetFones(const Value: TObjectList);
    procedure SetId(const Value: Integer);
    procedure SetIdade(const Value: Integer);
    procedure SetNome(const Value: String);
    function GetFones: TObjectList;
   public
    constructor Create;reintroduce;
    destructor Destroy;reintroduce;
   published
    property Id:Integer read FId write SetId;
    property Nome:String read FNome write SetNome;
    property Idade:Integer read FIdade write SetIdade;
    property Fones:TObjectList read GetFones write SetFones;
   End;

implementation

{ TTelefone }

procedure TTelefone.SetIdPessoa(const Value: Integer);
begin
  FIdPessoa := Value;
end;

procedure TTelefone.SetNumero(const Value: String);
begin
  FNumero := Value;
end;

{ TTelefones }

function TTelefones.GetItem(const Index: Integer): TTelefone;
begin
   Result := TTelefone(Self.Items[Index]);
end;

{ TPessoa }

constructor TPessoa.Create;
begin
   inherited Create;
end;

destructor TPessoa.Destroy;
begin
   FFones.Free;
   inherited Destroy;
end;

function TPessoa.GetFones: TObjectList;
begin
  if not Assigned(FFones) then
     FFones := TObjectList.Create;
  Result := FFones;   
end;

procedure TPessoa.SetFones(const Value: TObjectList);
begin
  FFones := Value;
end;

procedure TPessoa.SetId(const Value: Integer);
begin
  FId := Value;
end;

procedure TPessoa.SetIdade(const Value: Integer);
begin
  FIdade := Value;
end;

procedure TPessoa.SetNome(const Value: String);
begin
  FNome := Value;
end;

initialization RegisterClass(TPessoa);
               RegisterClass(TTelefone);
               ClassManager.RegisterServerClass(TTelefones);
end.

