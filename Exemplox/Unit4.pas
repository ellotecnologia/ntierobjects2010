unit Unit4;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ntoStreamImpl, Unit5, StdCtrls, xmldom, Xmlxform;

type
  TForm4 = class(TForm)
    ntoStream1: TntoStream;
    Memo1: TMemo;
    Memo2: TMemo;
    Button1: TButton;
    Button2: TButton;
    XMLTransform1: TXMLTransform;
    procedure FormCreate(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
  private
    { Private declarations }
    Pessoa:TPessoa;
  public
    { Public declarations }
  end;

var
  Form4: TForm4;

implementation


{$R *.dfm}


procedure TForm4.Button1Click(Sender: TObject);
var Tel:TTelefone;
begin
   Pessoa := TPessoa.Create;
   try
      Pessoa.Id := 1;
      Pessoa.Nome := 'Daniel Medeiros Paix�o';
      Pessoa.Idade := 30;
      {Residencial}
      Tel := TTelefone.Create;
      Tel.IdPessoa := 1;
      Tel.Numero := '30381786';
      Tel.Tipo := ttFixo;
      Pessoa.Fones.Add(Tel);
      {Celular}
      Tel := TTelefone.Create;
      Tel.IdPessoa := 1;
      Tel.Numero := '99343293';
      Tel.Tipo := ttCelular;
      Pessoa.Fones.Add(Tel);
      Memo1.Clear;
      Memo1.Lines.Add(ntoStream1.ToXML(Pessoa));
      Memo1.Lines.SaveToFile('Pessoa.xml');
   finally
      Pessoa.Free;
   end;
end;

procedure TForm4.Button2Click(Sender: TObject);
var Loop:Integer;
begin
  Pessoa := TPessoa.Create;
  Memo1.Lines.LoadFromFile('Pessoa.xml');
  try
     ntoStream1.FromXML(Pessoa,Memo1.Lines.Text);
  finally
     With Memo2.Lines do
     begin
        Clear;
        Add('Id: '+IntToStr(Pessoa.Id));
        Add('Nome: '+Pessoa.Nome);
        Add('Idade: '+IntToStr(Pessoa.Idade));
        Add('*** Fones ***');
        Add('Fixo');
        Add('Numero: '+TTelefone(Pessoa.Fones.Items[0]).Numero);
        Add('Celular');
        Add('Numero: '+TTelefone(Pessoa.Fones.Items[1]).Numero);
     end;
  end;
end;

procedure TForm4.FormCreate(Sender: TObject);
begin
//  ntoStream1.ToXML(Self);
end;

end.
