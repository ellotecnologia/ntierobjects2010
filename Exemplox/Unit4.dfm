object Form4: TForm4
  Left = 411
  Top = 284
  Width = 906
  Height = 530
  Caption = 'Form4'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Memo1: TMemo
    Left = 0
    Top = 0
    Width = 441
    Height = 492
    Align = alLeft
    Lines.Strings = (
      'Memo1')
    ScrollBars = ssVertical
    TabOrder = 0
    WantTabs = True
  end
  object Memo2: TMemo
    Left = 552
    Top = 0
    Width = 338
    Height = 492
    Align = alRight
    Lines.Strings = (
      'Memo2')
    TabOrder = 1
  end
  object Button1: TButton
    Left = 455
    Top = 72
    Width = 75
    Height = 25
    Caption = 'Exporta'
    TabOrder = 2
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 455
    Top = 128
    Width = 75
    Height = 25
    Caption = 'Importa'
    TabOrder = 3
    OnClick = Button2Click
  end
  object ntoStream1: TntoStream
    ExecucaoEmCascata = True
    ShowExportErrors = False
    ShowImportErrors = False
    Left = 320
    Top = 104
  end
  object XMLTransform1: TXMLTransform
    Left = 288
    Top = 168
  end
end
