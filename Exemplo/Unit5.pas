unit Unit5;

interface
   uses
      Contnrs, SysUtils, Classes, ntoStreamImpl, ClassManagerImpl;

Type
   TTipoTelefone = (ttCelular, ttFixo);

   TTelefone = Class(TCollectionItem) //Mudado de TObject pra TCollectionItem
   private
    FNumero: String;
    FIdPessoa: Integer;
    FTipo: TTipoTelefone;
    procedure SetIdPessoa(const Value: Integer);
    procedure SetNumero(const Value: String);
   public
    procedure Assign(Source:TPersistent);override;  //Falta isso
   published
    property IdPessoa:Integer read FIdPessoa write SetIdPessoa;
    property Numero:String read FNumero write SetNumero;
    property Tipo:TTipoTelefone read FTipo write FTipo;
   End;

   TTelefones = Class(TCollection)
   private
     function GetItem(const index:Integer):TTelefone;
   public
     constructor Create;//faltava isso
     function Add:TTelefone; //faltava isso
     property Items[const Index:Integer]:TTelefone read GetItem;default;
   End;

   TPessoa = Class(TPersistent)
   private
    FId: Integer;
    FIdade: Integer;
    FFones: TTelefones;
    FNome: String;
    procedure SetId(const Value: Integer);
    procedure SetIdade(const Value: Integer);
    procedure SetNome(const Value: String);
    function GetFones: TTelefones;
   public
    constructor Create;reintroduce;
    destructor Destroy;reintroduce;
   published
    property Id:Integer read FId write SetId;
    property Nome:String read FNome write SetNome;
    property Idade:Integer read FIdade write SetIdade;
    property Fones:TTelefones read GetFones write FFones; //Tirado o SetFones, nao precisa
   End;

implementation

{ TTelefone }

procedure TTelefone.Assign(Source: TPersistent);
begin
  inherited;
  FIdPessoa := TTelefone(Source).FIdPessoa;
  FNumero := TTelefone(Source).FNumero;
  FTipo := TTelefone(Source).FTipo;
end;

procedure TTelefone.SetIdPessoa(const Value: Integer);
begin
  FIdPessoa := Value;
end;

procedure TTelefone.SetNumero(const Value: String);
begin
  FNumero := Value;
end;

{ TTelefones }

function TTelefones.Add: TTelefone;
begin
   Result := TTelefone.Create(Self);
end;

constructor TTelefones.Create;
begin
   inherited Create(TTelefone);
end;

function TTelefones.GetItem(const Index: Integer): TTelefone;
begin
  Result := TTelefone(inherited GetItem(Index));
end;

{ TPessoa }

constructor TPessoa.Create;
begin
   inherited Create;
end;

destructor TPessoa.Destroy;
begin
   FFones.Free;
   inherited Destroy;
end;

function TPessoa.GetFones: TTelefones;
begin
  if (Not Assigned(FFones)) then
     FFones := TTelefones.Create;
  Result := FFones;   
  
end;

procedure TPessoa.SetId(const Value: Integer);
begin
  FId := Value;
end;

procedure TPessoa.SetIdade(const Value: Integer);
begin
  FIdade := Value;
end;

procedure TPessoa.SetNome(const Value: String);
begin
  FNome := Value;
end;

initialization RegisterClass(TPessoa);
               RegisterClass(TTelefone);
               ClassManager.RegisterServerClass(TTelefones);

end.

